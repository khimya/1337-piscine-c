/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/26 23:36:36 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/26 23:36:57 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int		*tab;
	int		size;
	int		d;
	int		i;

	if (min < max)
	{
		size = max - min;
		tab = (int*)malloc(sizeof(*tab) * (size));
		d = min;
		i = 0;
		while (d >= min && d < max)
		{
			tab[i] = d++;
			i++;
		}
		tab[i] = '\0';
		return (tab);
	}
	else
	{
		return (NULL);
	}
}
