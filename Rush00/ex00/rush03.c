/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoukhri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/13 19:44:10 by mmoukhri          #+#    #+#             */
/*   Updated: 2019/01/13 19:46:41 by mmoukhri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	drawing(int x, int y, int line, int column)
{
	while (line <= x)
	{
		if ((column == 1) && (line == 1))
			ft_putchar('A');
		else if (((column == y) && (line == 1)))
			ft_putchar('A');
		else if (((column == 1) && (line == x)))
			ft_putchar('C');
		else if ((column == y) && (line == x))
			ft_putchar('C');
		else if ((line > 1 && line < x) && (column > 1 && column < y))
			ft_putchar(' ');
		else
			ft_putchar('B');
		line++;
	}
}

void	rush(int x, int y)
{
	int line;
	int column;

	line = 1;
	column = 1;
	while (column <= y)
	{
		drawing(x, y, line, column);
		ft_putchar('\n');
		line = 1;
		column++;
	}
}
