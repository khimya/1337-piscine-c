/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/13 02:40:38 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/13 21:52:29 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrev(char *str)
{
	int		start;
	int		end;
	int		longeur;
	char	swapper;

	longeur = 0;
	while (str[longeur] != '\0')
	{
		longeur++;
	}
	start = 0;
	end = longeur - 1;
	while (start < end)
	{
		swapper = str[start];
		str[start] = str[end];
		str[end] = swapper;
		start++;
		end--;
	}
	return (str);
}
