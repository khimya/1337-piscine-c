/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 01:22:16 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/14 01:32:22 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	swapper(int *a, int *b)
{
	int c;

	c = *a;
	*a = *b;
	*b = c;
}

void	ft_sort_integer_table(int *tab, int size)
{
	int i;
	int j;

	i = 0;
	while (i < size - 1)
	{
		j = 0;
		while (j < (size - i - 1))
		{
			if (tab[j] > tab[j + 1])
			{
				swapper(&tab[j], &tab[j + 1]);
			}
			j++;
		}
		i++;
	}
}
