/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/13 22:03:30 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/14 01:35:59 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		num_check(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int		ft_atoi(char *str)
{
	int compteur;
	int sign;
	int result;

	sign = 1;
	compteur = 0;
	if (str[compteur] == '-')
	{
		sign = -sign;
		compteur++;
	}
	else if (str[compteur] == '+')
		compteur++;
	result = 0;
	while (str[compteur] != '\0' && num_check(str[compteur]))
	{
		result = result * 10 + (str[compteur] - 48);
		compteur++;
	}
	return (result * sign);
}
