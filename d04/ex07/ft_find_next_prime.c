/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 19:13:16 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/16 19:15:01 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_find_next_prime(int nb)
{
	int i;

	i = 1;
	if (nb <= 1)
	{
		return (2);
	}
	while (i < nb)
	{
		if (i != 1 && nb % i == 0)
		{
			nb++;
			i = 0;
		}
		i++;
	}
	return (nb);
}
