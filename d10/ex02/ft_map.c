/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 15:01:21 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/28 15:04:08 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int		i;
	int		*new_tab;

	i = -1;
	if (!(new_tab = (int *)malloc(sizeof(int) * length + 1)))
		return (NULL);
	while (++i < length)
		new_tab[i] = f(tab[i]);
	return (new_tab);
}
