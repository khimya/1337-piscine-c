/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/20 01:36:04 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/20 19:03:11 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int i;
	int c;

	i = 0;
	c = 1;
	while (str[i] != '\0')
	{
		if ((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'a'
					&& str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))
		{
			if (c && (str[i] >= 'a' && str[i] <= 'z'))
				str[i] -= 32;
			else if (!c && (str[i] >= 'A' && str[i] <= 'Z'))
				str[i] += 32;
			c = 0;
		}
		else
			c = 1;
		i++;
	}
	return (str);
}
