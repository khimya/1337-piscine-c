/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/19 21:24:45 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/20 19:29:04 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int i;

	i = 0;
	while (((int)s1[i] == (int)s2[i]) && ((s1[i] != '\0')
				&& (s2[i] != '\0')) && (i < (n - 1)))
	{
		i++;
	}
	if ((int)s1[i] == (int)s2[i])
	{
		return (0);
	}
	if ((int)s2[i] == '\0')
	{
		return (1);
	}
	if ((int)s1[i] == '\0')
	{
		return (-1);
	}
	if ((int)s1[i] > (int)s2[i])
	{
		return (1);
	}
	return (-1);
}
