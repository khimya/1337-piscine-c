/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_take_place.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 20:26:30 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/18 14:54:02 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_takes_place(int hour)
{
	write(1, "THE FOLLOWING TAKES PLACE BETWEEN ", 34);
	if (hour == 0)
		printf("12.00 A.M. AND 01.00 A.M.\n");
	if (hour == 12)
		printf("12.00 P.M. AND 01.00 P.M.\n");
	if (hour == 23)
		printf("11.00 P.M. AND 12.00 A.M.\n");
	if (hour == 11)
		printf("11.00 A.M. AND 12.00 P.M.\n");
	if (hour > 0 && hour < 11)
		printf("%d.00 A.M. AND  %d.00 P.M.\n", hour, hour + 1);
	if (hour > 12 && hour < 23)
		printf("%d.00 P.M. AND %d.00 P.M.\n", hour - 12, hour - 11);
}
