/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybenbrai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 17:52:00 by ybenbrai          #+#    #+#             */
/*   Updated: 2019/01/22 14:44:00 by ybenbrai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		main(int argc, char **argv)
{
	int i;
	int c;

	i = 0;
	c = 1;
	while (argc - c > 0)
	{
		while (argv[argc - c][i] != '\0')
		{
			ft_putchar(argv[argc - c][i]);
			i++;
		}
		ft_putchar('\n');
		i = 0;
		c++;
	}
	return (0);
}
